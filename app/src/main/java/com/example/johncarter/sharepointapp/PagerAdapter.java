package com.example.johncarter.sharepointapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Joane14 on 30/01/2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int numOfTabs;

    public PagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs=numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: CompleteFrag complete=new CompleteFrag();
                return complete;
            case 1: PendingFrag pending= new PendingFrag();
                return  pending;
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
