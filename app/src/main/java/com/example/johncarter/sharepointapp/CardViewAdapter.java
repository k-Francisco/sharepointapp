package com.example.johncarter.sharepointapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Joane14 on 14/02/2017.
 */

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> {
    private ArrayList<String> name, age, bDate;

    public CardViewAdapter(ArrayList<String> name, ArrayList<String> age, ArrayList<String> bdate){

        this.name=name;
        this.age=age;
        this.bDate=bdate;
    }

    @Override
    public CardViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_display, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CardViewAdapter.ViewHolder holder, int position) {
        holder.mName.setText(name.get(position));
        holder.mAge.setText(age.get(position));
        holder.mBDate.setText(bDate.get(position));

    }

    @Override
    public int getItemCount() {
        return name.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView mName, mAge, mBDate;
        public ViewHolder(View view){
            super(view);

            mName = (TextView) view.findViewById(R.id.tvName);
            mAge= (TextView) view.findViewById(R.id.tvAge);
            mBDate = (TextView) view.findViewById(R.id.tvBirthDate);
        }

    }

}