package com.example.johncarter.sharepointapp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joane14 on 02/02/2017.
 */

public class Person {
    String name;
    int age, photo;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getPhoto() {
        return photo;
    }

    public Person(){

    }

    public Person(String name, int age, int photo) {
        this.name = name;
        this.age = age;
        this.photo = photo;
    }

}

