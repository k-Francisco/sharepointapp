package com.example.johncarter.sharepointapp;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Joane14 on 31/01/2017.
 */

public class Timesheet extends Fragment {
    RecyclerView recyclerView;
    View v;
    private ArrayList name, age, bDate;
    public Timesheet() {

    }

    public static Timesheet newInstance() {
        Timesheet fragment = new Timesheet();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.timesheet_layout, container, false);
        this.v=v;

        initViews();


        return v;
    }

    private void initViews() {
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layout);

        name = new ArrayList<>();
        age = new ArrayList<>();
        bDate = new ArrayList<>();

        name.add("Joane Therese F. Artiaga");
        name.add("Charlyn Pepito");
        name.add("Jeon Jung Kook");
        name.add("Seo Joo Hyun");
        age.add("19");
        age.add("19");
        age.add("20");
        age.add("27");
        bDate.add("June 14, 1997");
        bDate.add("February 27, 1997");
        bDate.add("September 1, 1997");
        bDate.add("June 28, 1991");

        RecyclerView.Adapter adapter = new CardViewAdapter(name, age, bDate);
        recyclerView.setAdapter(adapter);


        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener(){

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    Log.d("Long Pressed", " is real~");
                }
            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if(child!=null && gestureDetector.onTouchEvent(e)){
                    int position = rv.getChildAdapterPosition(child);
                    Toast.makeText(getContext(), name.get(position)+", "+age.get(position)+" "+bDate.get(position), Toast.LENGTH_SHORT).show();
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        for(int init=0; init<name.size(); init++){
            Log.d("Name List: ", (String) name.get(init));
            Log.d("Age List: ", (String) age.get(init));
            Log.d("Birth Date List: ", (String) bDate.get(init));
        }
    }
}
