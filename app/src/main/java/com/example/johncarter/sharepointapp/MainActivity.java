package com.example.johncarter.sharepointapp;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.heinrichreimersoftware.materialdrawer.DrawerActivity;
import com.heinrichreimersoftware.materialdrawer.structure.DrawerItem;
import com.heinrichreimersoftware.materialdrawer.structure.DrawerProfile;
import com.heinrichreimersoftware.materialdrawer.theme.DrawerTheme;

@SuppressWarnings("ResourceType")
public class MainActivity extends DrawerActivity {
    private static final int CONTENT_VIEW_ID = 10101010;

    Fragment fragment;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment=null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        initToolbar();
        initDrawer();


    }

    private void initDrawer() {
        //setting the background and primary/secondary text of the drawer
        setDrawerTheme(
                new DrawerTheme(this)
                .setBackgroundColorRes(R.color.drawerBackground)
                .setHighlightColorRes(R.color.drawerHighLight)
        );

        //adding profile on the drawer
        addProfile(
                new DrawerProfile()
                        .setId(1)
                        .setName("Kristian Francisco")
                        .setDescription("piattosnovalays@gmail.com")
                        .setRoundedAvatar((BitmapDrawable) ContextCompat.getDrawable(this,R.drawable.cat_1))
                        .setBackground(ContextCompat.getDrawable(this,R.drawable.back2))
        );

        //adding items above the divider on the drawer
        addItems(
                new DrawerItem()
                        .setTextPrimary("Projects")
                        .setImage(ContextCompat.getDrawable(this,R.drawable.ic_action_project)),
                new DrawerItem()
                        .setTextPrimary("Approvals")
                        .setImage(ContextCompat.getDrawable(this,R.drawable.ic_action_checked_files)),
                new DrawerItem()
                        .setTextPrimary("Timesheet")
                        .setImage(ContextCompat.getDrawable(this,R.drawable.ic_action_project)),
                new DrawerItem()
                        .setTextPrimary("My Tasks")
                        .setImage(ContextCompat.getDrawable(this,R.drawable.ic_action_weekly_calendar)),
                new DrawerItem()
                        .setTextPrimary("Log out")
                        .setImage(ContextCompat.getDrawable(this,R.drawable.ic_action_log_out))
        );



        //handle drawer clicks
        setOnItemClickListener(new DrawerItem.OnItemClickListener() {
            @Override
            public void onClick(DrawerItem drawerItem, long id, int position) {
                selectItem(position);
                Log.d("Position: ", Integer.toString(position));
                FragmentManager fm = getSupportFragmentManager();
                Intent intent;

                if(position==0){
                    fragment = new createProject();
                    fm.beginTransaction().replace(R.id.activity_main, fragment).commit();
                }else if(position==1){
                    fragment = new approval();
                    fm.beginTransaction().replace(R.id.activity_main, fragment).commit();
                }else if(position==2){
                    fragment = new Timesheet();
                    fm.beginTransaction().replace(R.id.activity_main, fragment).commit();
                }else if(position==3){
                    fragment = new MyTasksFrag();
                    fm.beginTransaction().replace(R.id.activity_main, fragment).commit();
                }else if(position==4){
                    intent = new Intent(MainActivity.this, Log_In.class);
                    startActivity(intent);
                }

                if(Integer.toString(position)!=null&&position!=4){
                    Log.d("Position: ", "nisulod nga null daw ang position");
                    DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.mdDrawerLayout);
                    drawerLayout.closeDrawer(Gravity.START);
                }
            }
        });
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
}
