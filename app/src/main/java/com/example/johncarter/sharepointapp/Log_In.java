package com.example.johncarter.sharepointapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Log_In extends AppCompatActivity {
    EditText username;
    EditText password;
    TextView signup;
    Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log__in);

        username = (EditText)findViewById(R.id.txtUsername);
        password = (EditText)findViewById(R.id.txtPassword);
        signup = (TextView) findViewById(R.id.txtSignup);

        save = (Button)findViewById(R.id.btnSignIN);

        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(username.getText().toString().equals("Charlyn")&&
                        password.getText().toString().equals("2013")){
                    Toast.makeText(getApplicationContext(),"Redirecting....",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Log_In.this, MainActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(),"Wrong Credential",Toast.LENGTH_SHORT).show();
                    
                }
            }
        });
    }


}
